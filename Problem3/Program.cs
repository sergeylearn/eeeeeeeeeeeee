﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3
{
    public class Program
    {
        public static bool IsPrime(long i)
        {
            int count = 0;
            for (long j = 1; j <= i; j++)
            {
                if (i % j == 0)
                {
                    count = count + 1;
                }

            }
            return count == 2;
        }

        public static long LargestFactor(long n)
        {
            long lastFactor = 1;
            for (long i = 2; i <= n / 2; i++)
            {
                if (n % i == 0 && IsPrime(i))
                {
                    lastFactor = i;
                    Console.WriteLine(lastFactor);
                }
            }
            return lastFactor == 1 ? n : lastFactor;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(LargestFactor(600851475143));
            Console.ReadKey();
        }
    }
}
