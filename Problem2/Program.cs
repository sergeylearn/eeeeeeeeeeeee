﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = Solution();
            Console.WriteLine(sum);
            Console.ReadKey();
        }
        public static int Solution()
        {
            int f1 = 1;
            int f2 = 2;
            int s = 0;
            while (f2 < 4_000_000)
            {
                if (f2 % 2 == 0)
                {
                    s += f2;
                }
                int f = f1 + f2;
                f1 = f2;
                f2 = f;
                //Console.Write($"{f2} ");
            }
            return s;
        }
    }

}

