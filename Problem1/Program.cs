﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите предел");
            int limit = Convert.ToInt32(Console.ReadLine());
            int sum = Solution(limit);
            Console.WriteLine("сумма чисел кратных 3 или 5 = {0}", sum);
            Console.ReadKey();
        }
        public static int Solution(int limit)
        {
            int sum = 0;
            for (int i = 1; i < limit; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum = sum + i;
                }
            }
            return sum;
        }

    }
}
