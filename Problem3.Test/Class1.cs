﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Problem3;

namespace Problem3.Test
{
    [TestFixture]
    public class Test
    {

        [Test]
        public void Test1IsNotPrime()
        {
            Assert.That(Program.IsPrime(1), Is.False);
        }

        [Test]
        public void Test2IsPrime()
        {
            Assert.That(Program.IsPrime(2));
        }

        [Test]
        public void Test3IsPrime()
        {
            Assert.That(Program.IsPrime(3));
        }

        [Test]
        public void TestRandomPrimes()
        {
            var primes = new[]
            {                2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,
 61,67,71,73,79,83,89,97,101,103,107,109,113,127,
 131,137,139,149,151,157,163,167,173,179,181,191,
 193,197,199,211,223,227,229,233,239,241,251,257,
 263,269,271
            };
            foreach (var prime in primes)
            {
                Assert.That(Program.IsPrime(prime));
            }
        }

        [Test]
        public void TestLargestFactor()
        {
            var factors = new Dictionary<int, int>()
            {
                {2,2 },
                {3, 3 },
                {4, 2 },
                {6, 3 },
                {10, 5 },
                {27, 3 },
                {29, 29 },
                {100, 5 },
                {13195 ,29 },

            };

            foreach (var k in factors)
            {
                Assert.That(Program.LargestFactor(k.Key), Is.EqualTo(k.Value));
            }
        }

    }
}
